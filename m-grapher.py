'''
 Jack Dessert jnd31
 m-grapher.py
 created 11/14/2018
 Creates a logathmic plot of the payloads from the inputted traffic matrix file
 Basically just type in python3 m-grapher.py [traffic matrix file path]
 and the graph will be saved as the same file with the directories and file extension hacked off 
 and '.png' plastered on instead (strings in python are beautiful. Imagine this in C.)
 Also, I am CRYING from how beautiful python is
'''

import matplotlib.pyplot as plt
import numpy as np
import sys

def get_data(path):
	f = open(path, 'r')
	return f.readlines()

def grab_it_by_the_payload(data):
	for i in range(len(data)):
		data[i] = int(data[i].split()[2])
	return data


def sort_and_graph(data, filepath):
	data.sort(reverse = True)
	plt.plot(np.arange(len(data)), data, '-o')
	plt.yscale("log")
	plt.xscale("log")
	plt.ylabel('Payloads')
	plt.xlabel('Payload rank in sorted order')
	plt.grid()
	plt.savefig(filepath.split("/")[-1].split(".")[0]+'.png')


def main():
	if len(sys.argv) != 2:
		raise Exception("Requires one argument: file path")
	data = get_data(str(sys.argv[1]))
	sort_and_graph(grab_it_by_the_payload(data), sys.argv[1])


if __name__ == "__main__":
	main()
